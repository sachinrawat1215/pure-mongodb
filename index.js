console.log("MongoDB Tutorial");



const mongo = require('mongodb');

// To create a database in MongoDB ----->
var MongoClient = require('mongodb').MongoClient;

// URL for MongoDB ----->
var url = "mongodb://localhost:27017/mydb";




// Creating a Database ----->

// MongoClient.connect(url, function (err, db) {
//     if (err) throw err;
//     console.log("Database created!");
//     db.close();
// });





// Creating a Collection ----->

// MongoClient.connect(url, function (err, db) {
//     if (err) throw err;

//     const dbo = db.db("mydb");
//     dbo.createCollection("customers", function(err, res) {
//         if(err) throw err;
//         console.log("Collection Created");
//         db.close();
//     })
// });





// Insert Into Collection  ----->

// MongoClient.connect(url, function (err, db) {
//     if (err) throw err;
//     const dbo = db.db("mydb");

//     var myobj = { name: "Company Inc", address: "Highway 37" };

//     dbo.collection("customers").insertOne(myobj, function(err, res){
//         if(err) throw err;
//         console.log("1 document added" + res.insertedCount);
//         db.close;
//     })
// });





// Insert Multiple Documents ----->

// MongoClient.connect(url, function(err, db) {
//     if(err) throw err;

//     const dbo = db.db("mydb");

//     var myobj = [
//         { name: 'John', address: 'Highway 71'},
//         { name: 'Peter', address: 'Lowstreet 4'},
//         { name: 'Amy', address: 'Apple st 652'},
//         { name: 'Hannah', address: 'Mountain 21'},
//         { name: 'Viola', address: 'Sideway 1633'}
//       ];

//       dbo.collection("customers").insertMany(myobj, function(err, res) {
//           if(err) throw err;
//           console.log("Number of document inserted: " + res.insertedCount);
//           db.close();
//       })
// })





// Inserting Custom _id for document

// MongoClient.connect(url, function(err, db){
//     if(err) throw err;

//     const dbo = db.db("mydb");

//     const obj = [
//         { _id: 157, name: 'Chocolate Heaven'},
//         { _id: 158, name: 'Tasty Lemon'},
//         { _id: 159, name: 'Vanilla Dream'}
//       ];

//       dbo.collection("customers").insertMany(obj, function(err, res){
//           if(err) throw err;
//           console.log(res);
//           db.close();
//       })
// });